#!/bin/bash
# Backup Script
#=====================================================================
# Directory to save daily tar.gz backup files to e.g /backups
BACKUPDIR="/backups"

# Directory to backup
BACKUPSRCDIR=$1
SERVICE=$(echo $BACKUPSRCDIR |rev| cut -d '/' -f 1 |rev)

# Maximum size of backup files in MB (larger files will be split into separate archives)
# Note: not implemented yet
MAXTARSIZE="1000"

# S3 Settings
# The name of the S3 bucket to upload to e.g. "my_s3_bucket"
S3BUCKET=$2

# Should not need to be modified from here down!!
#=====================================================================
PATH=/usr/local/bin:/usr/bin:/bin:
DATE=`date +%Y-%m-%d_%Hh%Mm`                            # Datestamp e.g 2002-09-21
DOW=`date +%A`                                          # Day of the week e.g. Monday
DNOW=`date +%u`                                         # Day number of the week 1 to 7 where 1 represents Monday
DOM=`date +%d`                                          # Date of the Month e.g. 27
M=`date +%B`                                            # Month e.g January
W=`date +%V`                                            # Week Number e.g 37

# Create required directories
if [ ! -e "$BACKUPDIR" ]                # Check Backup Directory exists.
then
mkdir -p "$BACKUPDIR"
fi

# Functions

# Backup function: removes last weeks archive from S3, creates new tar.gz and sends to S3
dobackup () {
s3cmd ls s3://"$S3BUCKET"/$SERVICE | grep s3 | sed "s/.*s3:\/\/$S3BUCKET\//s3:\/\/$S3BUCKET\//" | grep "$DOW" | xargs s3cmd del
tar cfz "$1" "$2"
echo
echo Backup Information for "$1"
gzip -l "$1"
echo
s3cmd put "$1" s3://"$S3BUCKET"
return 0
}
# Daily Backup
#echo Rotating last weeks Backup...
eval rm -fv "$BACKUPDIR/$SERVICE-*.$DOW.tar.gz"

dobackup "$BACKUPDIR/$SERVICE-$DATE.$DOW.tar.gz" "$BACKUPSRCDIR"
